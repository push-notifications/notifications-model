package main

import (
	"crypto/md5"
	"fmt"
	"time"

	"github.com/google/uuid"
)

// Model the 1st half of the notifications system, from notification channels to subscribers.
// Notifications enter the system from channels and must be routed to all their destinations (subscribers)
// From each subscriber, they must then be routed to appropriate devices and receipts be backpropagated

// Notification is the central currency
type Notification struct {
	uuid.UUID
	timestamp time.Time
	source    Identity
	Channel
	Content string
}

// Identity in the AuthzSvc sense: a CERN Identity
type Identity interface {
}

// Channel is an entity that subscribers can listen to and that defines message routing
type Channel interface {
	Broadcast(Notification)
}

type Subscriber interface {
	Identity
	RecvQueue
}

type RecvQueue interface {
	Receive(Notification)
}

func route(n Notification) {
	// Send to all n.Channel subscribers apart from n.source
	n.Channel.Broadcast(n)
}

func main() {
	notifC := make(chan Notification)
	go generateRandomNotifications(notifC, 5)
	for n := range notifC {
		route(n)
	}
	return
}

// MOCK

func generateRandomNotifications(notifC chan Notification, nGenerate int) {
	for i := 0; i < nGenerate; i++ {
		notifC <- randomNotification()
	}
	close(notifC)
}

func randomNotification() Notification {
	return Notification{
		UUID:      uuid.New(),
		timestamp: time.Now(),
		Channel:   randomChannel(),
		Content:   randomString(),
	}
}

type NamedChannel struct {
	Name        string
	subscribers []Subscriber
}

func (channel *NamedChannel) Broadcast(n Notification) {
	for _, s := range channel.subscribers {
		fmt.Println("Notification: " + n.UUID.String())
		s.Receive(n)
	}
}

func randomChannel() *NamedChannel {
	return &NamedChannel{
		Name:        randomString(),
		subscribers: randomSubscribers(5),
	}
}

func randomString() string {
	t, _ := time.Now().GobEncode()
	return fmt.Sprintf("%s", md5.Sum(t))
}

func randomSubscribers(n int) []Subscriber {
	s := make([]Subscriber, n)
	for i := 0; i < n; i++ {
		s[i] = &NamedSubscriber{
			Identity:  randomIdentity(),
			RecvQueue: &ChanQueue{make(chan Notification, 2)},
		}
	}
	return s
}

func randomIdentity() Identity {
	return NamedChannel{
		Name: "hello",
	}
}

type ChanQueue struct {
	q chan Notification
}

func (cq *ChanQueue) Receive(n Notification) {
	cq.q <- n
	fmt.Println(len(cq.q))
	return
}

type NamedSubscriber struct {
	Identity
	RecvQueue
}
